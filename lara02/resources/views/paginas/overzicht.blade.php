@extends('sjabloon.land')


@section('inhoud')
    <div class="container">
        <div class="row">
            <div id="inhoud">
                <h1>Landen: lijst </h1>
                @if (count($landen) === 0)
                    <div class="alert alert-warning">
                        <h3>
                            <i class="bi bi-exclamation-triangle">
                                Opgelet! Lijst met landen is leeg.
                            </i>
                        </h3>
                    </div>
                @else
                    <div class="accordion accordion-flush" id="landInfo">
                        @foreach ($landen as $landIndex => $land)
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="land_{{ $landIndex }}">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#land_{{ $landIndex }}_detail" aria-expanded="false"
                                        aria-controls="land_{{ $landIndex }}">
                                        {{ $land->land }} | &nbsp; <small> {{ $land->iso }}</small>
                                    </button>
                                </h2>
                                <div id="land_{{ $landIndex }}_detail" class="landDetail accordion-collapse collapse"
                                    aria-labelledby="landInfo" data-bs-parent="#landInfo">
                                    <div class="accordion-body">
                                        <div class="landDetailItem">
                                            <label>Oppervlakte:</label>
                                            {{ $land->oppervlakte }} km <sup>2</sub>
                                        </div>
                                        <div class="landDetailItem">
                                            <label>Inwoners:</label>
                                            {{ $land->inwoners }} 
                                        </div>

                                        <div class="landDetailItem">
                                            <label>Hoofdstad:</label>
                                            {{ $land->hoofdstad }} 
                                        </div>

                                        <div class="landDetailItem">
                                            <label>Staatshoofd:</label>
                                            {{ $land->staatshoofd }} 
                                        </div>

                                        <div class="landDetailItem float-end">
                                            <button class="btn btn-primary landBewerk" data-landid="{{ $land->id }}">
                                                <i class="bi bi-pencil-square"></i> &nbsp; Bewerk
                                            </button>
                                            <button class="btn btn-warning landVerwijder" data-landid="{{ $land->id }}">
                                                <i class="bi bi-x-square"></i> &nbsp; Verwijder
                                            </button>
                                        </div>

                                        <!-- clearfix is for the float-end above float-end is on the right end?-->
                                        <div class="clearfix"></div>

                                    </div>
                                </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
