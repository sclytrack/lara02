@extends('sjabloon.land')

@section('inhoud')
<div class="container">
    <div class="row">
        <div class="inhoud">
            <h1>Landen: {{ $actie }}</h1>

            @if (isset($fouten) && $fouten)
               <div class="alert alert-warning">
                  <h4 class="alert-heading">
                     <i class="bi bi-exclamation-triangle"></i>
                     Oeps...
                  </h4>
                  <ul>
                  @foreach( $fouten->all() as $fout)
                    <li>
                            {{ $fout }}
                    </li>
                  @endforeach
                  </ul>
               </div>
            @endif


            <?php
              $ro = $actie === 'verwijder'? 'readonly': '';
              $btn = $actie === 'verwijder' ? 'Verwijder' : 'Bewaar';
            ?>

            <form action="/bewaar" method="post" id="frmDetail">
                {{ csrf_field() }}
                <input type="hidden" name="landID" value="{{ $land->id }}">
                <input type="hidden" name="actie" value = "{{ $actie }}">

                <div class="mb-3">
                    <label class="form-label">Land</label>
                    <input type="text" class="form-control" name="land" value="{{ $land->land }}" {{ $ro }}>
                </div>

                <div class="mb-3">
                    <label class="form-label">ISO:</label>
                    <input type="text" class="form-control" name="iso" value="{{ $land->iso }}" {{ $ro }}>
                </div>

                <div class="mb-3">
                    <label class="form-label">Oppervlakte<sup>2</sup></label>
                    <input type="text" class="form-control" name="oppervlakte" value="{{ $land->oppervlakte }}" {{ $ro }}>
                </div>
                <div class="mb-3">
                    <label class="form-label">Inwoners</label>
                    <input type="text" class="form-control" name="inwoners" value="{{ $land->inwoners }}" {{ $ro }}>
                </div>
                <div class="mb-3">
                    <label class="form-label">Hoofdstad</label>
                    <input type="text" class="form-control" name="hoofdstad" value="{{ $land->hoofdstad }}" {{ $ro }}>
                </div>
                <div class="mb-3">
                    <label class="form-label">Staatshoofd</label>
                    <input type="text" class="form-control" name="staatshoofd" value="{{ $land->staatshoofd }}" {{ $ro }}>
                </div>

                <div class="mb-3 float-end">
                    <button class="btn btn-primary" type="button" id="btnBewaar">
                        <i class="bi bi-check-square">&nbsp</i> {{ $btn }}
                    </button>
                    <button class="btn btn-secondary" type="button" id="btnAnnuleer">
                        <i class="bi bi-x-square">&nbsp</i> Annuleer
                    </button>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection