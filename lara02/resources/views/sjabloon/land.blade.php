<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{-- This is a comment in the blade page --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- This is the icon that appears on the browser tab --}}
    <link rel = "shortcut icon" type="image/png" href="{{ URL::to('afbeeldingen/logo.png') }}">
    <title>{{ config('app.name', 'call Bram for help') }}</title>
</head>
    <script src="{{ URL::to('js/jquery-3.6.1.min.js') }}"></script>
    @vite(['resources/js/app.js'])
<body>
    @include('include.kop')
    @yield('inhoud')
</body>
</html>