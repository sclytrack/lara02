<div id="kop">
    <nav class="navbar">
        <div class="container">
            <a href="#" class="navbar-brand">
                <img src="{{  URL::to('afbeeldingen/logo.png') }}">
                <h1>{{  config('app.name', '') }}</h1>
            </a>

            @if ($btnNieuw)
                <div class="ms-right">
                    <button id="landNieuw" class="btn btn-primary">
                        <i class="bi bi-plus-square"></i> &nbsp; Nieuw
                    </button>
                </div>
            @endif
        </div>
    </nav>
</div>