$(() => {

  if ($('#landInfo')) OVERZICHT.init();

  if ($('#landNieuw'))
  {
    $('#landNieuw').on('click', () => {
      OVERZICHT.bewerk(0, 'nieuw');
    })
  }

  if ($('#detail'))
  {
    DETAIL.init();
  }

});

const OVERZICHT = {
    init: () => {
        $('.landBewerk').on('click', function(evt) {
            let landID = $(this).data('landid');
            console.log('bewerk',landID);
            OVERZICHT.bewerk(landID, 'bewerk')
        });
        $('.landVerwijder').on('click', function(evt) {
            let landID = $(this).data('landid');
            console.log('verwijder',landID);
            OVERZICHT.bewerk(landID, 'verwijder')
        });
    },
    bewerk: (landID, actie) => {
        window.location = `/detail/${landID}/${actie}`;
    }
}



const DETAIL = {
  init: () => {
    $("#btnAnnuleer").on('click', () => {DETAIL.overzicht();});
    $("#btnBewaar").on('click', () => {DETAIL.bewaar();});
  },

  overzicht: () => {
    window.location  = '/';
  },

  bewaar: ()=> {
    $('#frmDetail').submit();
  }

}