<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


//  Created with php artisan make:model land (without s)

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('land', 100)->nullable()->default('');
            $table->string('iso', 3)->default('');
            $table->unsignedInteger('oppervlakte')->default(0);
            $table->unsignedInteger('inwoners')->default(0);
            $table->string('hoofdstad', 100)->default('');
            $table->string('staatshoofd', 100)->default('');
            $table->timestamps();

            # primary key
            $table->index('id');
            # unique constraint
            $table->unique('land');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landen');
    }
};
