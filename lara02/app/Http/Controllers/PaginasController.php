<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Land;         //php artisan make:model land

use Validator;

//The page controller defines the routes.

class PaginasController extends Controller
{
    public function overzicht()
    {
        //Land::orderBy('land', 'asc')->paginate(),
        $data = ['landen'=> Land::orderBy('land', 'asc')->get(),
                'land' => null,
                'actie' => null,
                'btnNieuw'=>true,
                'boodschap'=> ''];

        return view(sprintf("paginas.overzicht"))->with($data);
    }

    public function bewerk($landID=0, $actie='')
    {
        if ($actie == 'bewerk' || $actie == 'verwijder')
        {
            if ($landID === 0) {
                return redirect('/');
            }
            $land = Land::find($landID); //normally get added?
        }
        else    //nieuw
        {
            $land = new Land();
        }
//        $data = ['btnNieuw'=>false];

        $data = ['landen'=> null,
        'land' => $land,
        'actie' => $actie,
        'btnNieuw'=>false,
        'boodschap'=> ''];


        return view('paginas.detail')->with($data);
    }

    public function bewaar(Request $request)
    {
        $actie = $request->input('actie');

        if ($actie === 'verwijder')
        {
          Land::find($request->input('landID'))->delete();
        }
        else
        {
          //bewerk of nieuw
          $valideerVoorwaarden = [
            //'land' => 'required|max:100|unique:landen',  //can not use unique for update.
            'land' => 'required|max:100',
            'iso' => 'required|regex:/^[A-Z]{2}$/',
            'oppervlakte' => 'required|numeric|gt:0',
            'inwoners' => 'required|numeric|gt:0',
            'hoofdstad' => 'required|max:100',
            'staatshoofd' => 'required|max:100'
          ];

          if ($actie == 'nieuw')
          {
            $valideerVoorwaarden['land'] .= '|unique:landen';
          }

          /*
          if ($actie == 'bewerk')
          {
            //array_unshift($valideerVoorwaarden, ['land' => 'required|max:100']); //no unique check for update

            $valideerVoorwaarden['land'] = 'required|max:100';
          } else
          {
            //array_unshift($valideerVoorwaarden, ['land' => 'required|max:100|unique:landen']);
            $valideerVoorwaarden['land'] = 'required|max:100|unique:landen';
          }
          */

          $valideerBoodschappen = [
            'land.required' => 'Land is een verplicht veld',
            'land.max' => 'Maximum 100 karakters voor land',
            'land.unique' => 'Land moet uniek zijn',
            'iso.required' => 'Iso is een verplicht veld',
            'iso.regex' => 'Iso bestaat uit precies twee hoofdletters',
            'opervlakte.required' => 'Oppervlakte is verplicht',
            'oppervlakte.gt' => 'Oppervlakte moet groter zijn dan 0',
            'oppervlakte.numeric' => 'Oppervlakte moet een getal zijn',
            'inwoners.required' => 'Het aantal inwoners is verplicht',
            'inwoners.gt' => 'Inwoners moet groter zijn dan 0', //10
            'inwoners.numeric' => 'Inwoners moet een getal zijn',
            'hoofdstad.required' => 'Hoofdstad is verplicht',
            'hoofdstad.max' => 'Maximum 100 letters voor de hoofdstad',
            'staatshoofd.required' => 'Staatshoofd is verplicht',
            'staatshoofd.max' => 'Maximum 100 letters voor de hoofdstad'
          ];


          $valideer = Validator::make($request->all(), $valideerVoorwaarden, $valideerBoodschappen);


          if ($request->input('landID') > 0)
          {
            //Bestaand land updaten
            $land = Land::find($request->input('landID'));
          }
          else
          {
            $land = new Land();
          }

          //request not callable
          
          $land->land = $request->input('land');
          $land->iso = $request->input('iso');
          $land->inwoners = $request->input('inwoners');
          $land->oppervlakte = $request->input('oppervlakte');
          $land->staatshoofd = $request->input('staatshoofd');
          $land->hoofdstad = $request->input('hoofdstad');

          if ($valideer->fails())
          {
            //print_r($valideer->messages()); die();

            $dta = ['landen'=>null,
                 'land' => $land,
                 'btnNieuw' => false,
                 'actie' => $actie,
                 'fouten' => $valideer->messages()];

            return view('paginas.detail')->with($dta);

          } elseif ($actie == 'bewerk')
          {
            //bestaand land wijzigen --> update
            $land->update();

          } 
          else
          {
            //nieuw land -> insert

            $land->save();
          }

        }
        return redirect('/');
    }


}
