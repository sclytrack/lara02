<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PaginasController;

//  lara02.local/
//  lara02.local/detail/{land ID}/{actie}
//  lara02.local/bewaar


// Lijst van landen
Route::get('/', [PaginasController::class, 'overzicht']);
// formulier  shift -alt pijltje naar beneden
Route::get('/detail/{landID}/{actie}', [PaginasController::class, 'bewerk']);
Route::post('/bewaar', [PaginasController::class, 'bewaar']);


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*
Route::get('/', function () {
    return view('welcome');
});
*/
