import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

import path from 'path';


export default defineConfig({
    server: {
        https: true,
        host: 'lara02.local'
    },
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss',
                'resources/js/app.js',
            ],
            refresh: true,
        }),
    ],
    resolve: {
        alias: {
            '~bootstrap': path.resolve(__dirname, 'node_modules/bootstrap'),
            '~bootstrap-icons': path.resolve(__dirname, 'node_modules/bootstrap-icons')
        }
    }
});
